
# coding: utf-8

# In[1]:

from keras.models import Sequential
from keras.layers import Dense, TimeDistributed, GRU, LSTM
from keras.callbacks import ModelCheckpoint, EarlyStopping, Callback
import numpy as np
import sys
import os
import pickle, gzip
#from keras.utils.visualize_util import plot
#import gen_lstm_serial_input as serial
#import xormachine as xm
#import serialemu
import json
import random
import time
import platform


#
X, Y = pickle.load(gzip.open("lstm-serial-dataset-1000-20000.p.gz", 'rb'))


# In[6]:

def_options = {
    'machines': ['SerialPortMachine'],
    'nhidden': [0,1,2,5,10],
    'activations': ['tanh', 'sigmoid', 'linear'],
    'training_size': [20000],
    'sequence_len': [1000],
    'batch_size': [1, 5, 20, 50],
    'epochs': [20],
    'type': ['gru'],
    'optimizers': ['adagrad', 'adadelta', 'rmsprop', 'adam', 'nadam', 'adamax'],
    'loss': ['mse', 'mae', 'mape', 'msle']
}

def gen_worklist(opts={}):
    # 
    # First, load defaults and overwite them with things in the opts dictionary
    #
    our_opts = def_options
    for k in opts.keys():
        our_opts[k] = opts[k]
    
    ret = []
    for m in our_opts['machines']:
        for n in our_opts['nhidden']:
            for a in our_opts['activations']:
                for t in our_opts['training_size']:
                    for s in our_opts['sequence_len']:
                        for b in our_opts['batch_size']:
                            for e in our_opts['epochs']:
                                for y in our_opts['type']:
                                    for o in our_opts['optimizers']:
                                        for l in our_opts['loss']:

                                            experiment = { 'type': y, 'machine': m, 'nhidden': n, 'actfunc': a, 'N': t, 'seqlen': s, 'batchsize': b, 'nepochs': e, 'loss': l, 'opt': o }
                                            ret.append(json.dumps(experiment))
    
    random.shuffle(ret)
    return ret


#worklist = gen_worklist(opts = {})
#print(len(worklist))
#print(worklist[0])


# In[8]:

def validate_random_rounded(model, machine, seqlen, num_trials):
    gen_func = mach_types[machine][1]
    correct = 0
    #attempt to track the average number of errors for each column (output type)
    err_mats = []
    err_round_mats = []
    #avg_error_counts = []
    for _ in range(num_trials):
        Xt, Yt = gen_func(mach_types[machine][0], seqlen, 1)
        res_raw = model.predict(Xt, batch_size=1, verbose=0)
        
        err_mats.append((res_raw[0] - Yt[0]).tolist())
        
        res = np.rint(res_raw).astype(int)
        diff = (res[0] == Yt[0])
        
        err_round_mats.append(diff.tolist())
        
        if np.all(diff):
            correct += 1
        #else:
        #    for i in range(0,diff.shape[1]):
        #        if len(avg_error_counts) != diff.shape[1]:
        #            avg_error_counts.append(np.count_nonzero(diff[:,i]  == False))
        #        else:
        #            avg_error_counts[i] += np.count_nonzero(diff[:,i] == False)
    
    #return (correct, avg_error_counts, err_mats, err_round_mats)
    return (correct, err_mats, err_round_mats)
    
class val_random_callback(Callback):
    def __init__(self, machine, seqlen, ntest):
        self.seqlen = seqlen
        self.machine = machine
        self.ntest = ntest
        self.hist = []
        self.pct_right = []
        
    def on_epoch_end(self, epoch, logs={}):
        if epoch % 5 == 0:
            print('Evaluating Random Callback: ', end="")
            ret = validate_random_rounded(self.model, self.machine, self.seqlen, self.ntest)
            print('%d/%d predicted correctly' % (ret[0], self.ntest))
            
            self.pct_right.append(float(float(ret[0])/float(self.ntest)))
            self.hist.append(ret)
    


# In[9]:

def build_model_srnn(n_in, n_out, seqlen, metrics=["accuracy"], nhidden=0, actfunc="tanh", opt='adam', loss='mse'):
    model = Sequential()
    model.add(SimpleRNN(n_in, input_dim=n_in, 
                input_length=seqlen, 
                activation=actfunc,
                return_sequences=True, consume_less='cpu'))
    for _ in range(nhidden):
        model.add(SimpleRNN(max(n_in,n_out)+1, return_sequences=True, consume_less='cpu', activation=actfunc))
    model.add(TimeDistributed(Dense(n_out, activation=actfunc)))
    model.compile(loss=loss, optimizer=opt, metrics=metrics)
    return model

def build_model_gru(n_in, n_out, seqlen, metrics=["accuracy"], nhidden=0, actfunc="tanh", opt='adam', loss='mse'):
    model = Sequential()
    model.add(GRU(n_in, input_dim=n_in, 
                input_length=seqlen, 
                activation=actfunc,
                return_sequences=True, consume_less='gpu'))
    for _ in range(nhidden):
        model.add(GRU(max(n_in,n_out)+1, return_sequences=True, consume_less='gpu', activation=actfunc))
    model.add(TimeDistributed(Dense(n_out, activation=actfunc)))
    model.compile(loss=loss, optimizer=opt, metrics=metrics)
    return model

def build_model_lstm(n_in, n_out, seqlen, metrics=["accuracy"], nhidden=0, actfunc="tanh", opt='adam', loss='mse'):
    model = Sequential()
    model.add(LSTM(n_in, input_dim=n_in, 
                input_length=seqlen, 
                activation=actfunc,
                return_sequences=True, consume_less='cpu'))
    for _ in range(nhidden):
        model.add(LSTM(max(n_in,n_out)+1, return_sequences=True, consume_less='cpu', activation=actfunc))
    model.add(TimeDistributed(Dense(n_out, activation=actfunc)))
    model.compile(loss=loss, optimizer=opt, metrics=metrics)
    return model

def build_model_dense(n_in, n_out, seqlen, metrics=["accuracy"], nhidden=0, actfunc="tanh", opt='adam', loss='mse'):
    model = Sequential()
    model.add(TimeDistributed(Dense(n_in, activation=actfunc), input_shape=(seqlen,n_in)))
    for _ in range(nhidden):
        model.add(TimeDistributed(Dense(max(n_in,n_out)+1, activation=actfunc)))
    model.add(TimeDistributed(Dense(n_out, activation=actfunc)))
    model.compile(loss=loss, optimizer=opt, metrics=metrics)
    return model


# In[10]:

def run_experiment_json(e):
    print(e)
    e = json.loads(e)
    res = run_experiment(e['machine'], e['type'], e['N'], e['seqlen'], 
                  e['batchsize'], e['nepochs'], 
                  actfunc=e['actfunc'], nhidden=e['nhidden'],
                  opt=e['opt'], loss=e['loss'])
    return json.dumps(res)
    
def run_experiment(machine, nettype, N, seqlen, batch_size, nepochs, actfunc="tanh", nhidden=3, opt='adam', loss='mse'):

    netbuilders = {
        'lstm': build_model_lstm,
        'gru': build_model_gru,
        'srnn': build_model_srnn,
        'dense': build_model_dense
    }
    ret = {}
    
    print("Running experiment on %s (%d,%d) (type=%s, nidden=%d, act=%s, opt=%s, loss=%s) (batch=%d, nep=%d)" %
                     (machine,N,seqlen,nettype,nhidden,actfunc,opt,loss,batch_size,nepochs))
    
    # Create the output directory
    outdir = 'output/%s/%s/%d/%d/nh%d-act%s/opt%s-loss%s/bs%d-ep%d' % (machine,nettype,N,seqlen,nhidden,actfunc,opt,loss,batch_size,nepochs)
    print("Output saved to: %s" % (outdir,))
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    log = open(os.path.join(outdir, 'log.txt'), 'wt')
    begin = time.time()
    log.write('Time at start: %f\n' % (begin,))
    log.write("Running experiment on %s (%d,%d) (type=%s, nidden=%d, act=%s, opt=%s, loss=%s) (batch=%d, nep=%d)" %
                     (machine,N,seqlen,nettype,nhidden,actfunc,opt,loss,batch_size,nepochs))

    model = netbuilders[nettype](X.shape[2], Y.shape[2], seqlen, nhidden=nhidden, actfunc=actfunc, opt=opt, loss=loss)
    model.summary()

    #val_cb = val_random_callback(machine, seqlen, 100) 
    mc = ModelCheckpoint(filepath=os.path.join(outdir,'model.weights.{epoch:02d}-{val_loss:.5f}.hdf5'))
    print("Training")
    hist = model.fit(X, Y, nb_epoch=nepochs, batch_size=batch_size, 
                     validation_split=0.10, callbacks=[mc], verbose=2)
    
    end = time.time()
    print("Training Finished: %f" % (end-begin))
    log.write("Elapsed time: %f\n" % (end-begin))
    
    #print("Validating Final Model")
    #final_val = validate_random_rounded(model, machine, seqlen, 1000)
    #print("%d / %d (%f)" % (final_val[0], 1000, float(float(final_val[0])/float(1000))))
    #log.write("Final Val: %d / %d (%f)\n" % (final_val[0], 1000, float(float(final_val[0])/float(1000))))
    
    #log.write("Pct Right: %s\n" % (str(val_cb.pct_right)))
    
    model.save(os.path.join(outdir,'model.final.hd5'))
    with open(os.path.join(outdir,'history.p'), 'wb') as h:
        pickle.dump(hist.history, h)
        
    #with open(os.path.join(outdir,'valcb.p'), 'wb') as vcb:
    #    pickle.dump((val_cb.hist, val_cb.pct_right), vcb)
    
    ret['type'] = nettype 
    ret['machine'] = machine
    ret['N'] = N
    ret['nhidden'] = nhidden
    ret['seqlen'] = seqlen
    ret['nepoch'] = nepochs
    ret['batchsize'] = batch_size
    ret['actfunc'] = actfunc
    ret['lossfunc'] = loss
    ret['opt'] = opt
    ret['time'] = end-begin
    #ret['valcb_pct'] = val_cb.pct_right
    #ret['valcb_hist'] = val_cb.hist
    ret['last5_loss'] = hist.history['loss'][-5:]
    #ret['final_val_out_of_1000'] = final_val[0]
    
    for k in hist.history.keys():
        ret[k] = hist.history[k]
        ret['final_%s' % (k,)] = hist.history[k][-1]
        #log.write('' + k + ' = ' + hist.history[k][-1] + '\n')
    
    with open(os.path.join(outdir, 'result.json'), 'wt') as r:
        tmp = json.dumps(ret)
        r.write(tmp)
        log.write('' + tmp + '\n')
        
    log.close()
    print("Done processing, success.")
    return ret;
    

