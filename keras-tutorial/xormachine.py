import random
import numpy as np
import copy

#
# Simple machine that returns the last value written on input to
# the output.
#
class EightBitMachine():
	def __init__(self):
		self.count = 0
		self.inputs = [0]*8

	def set_input(self, r):
		self.inputs[r] = 1

	def clear_input(self, r):
		self.inputs[r] = 0

	def get_input(self, r):
		return self.inputs[r]

	def output(self):
		return copy.copy(self.inputs)

	def reset(self):
		self.inputs = [0]*8

#
# Machine that takes 8 inputs and ignores 7 of them except the 
# input specified. 
#
class SingleDirectMachine(EightBitMachine):
	def __init__(self, r):
		EightBitMachine.__init__(self)
		self.mybit = r

	def output(self):
		ret = [0]*8
		ret[self.mybit] = self.inputs[self.mybit]
		return ret

# 
# Machine with take 8 input bits, and inverts one output based on
# what was wirtten to the specified input last.  
# All other inputs ignored.
#
class SimpleDirectInvertMachine(EightBitMachine):
	def __init__(self, r):
		EightBitMachine.__init__(self)
		self.mybit = r

	def output(self):
		ret = [0]*8
		ret[self.mybit] = abs(self.inputs[self.mybit]-1)
		return ret

# 
# Machine that inplements XOR between the two inputs specified. All
# other inputs ignored. 
#

class SimpleXORMachine(EightBitMachine):
	def __init__(self, i1, i2):
		EightBitMachine.__init__(self)
		self.i1 = i1
		self.i2 = i2
	
	def output(self):
		return [1 if bool(self.inputs[self.i1]) ^ bool(self.inputs[self.i2]) else 0]	

#
# Machine that returns the parity of the eight bits set. 0 for even, 1 for odd. 
#
class ParityMachine(EightBitMachine):
	def __init__(self):
		EightBitMachine.__init__(self)

	def output(self):
		return [0 if sum(self.inputs) % 2 == 0 else 1]

#
# Next several func generate test data.  Returns (X, Y), both 3D matricies, 
# where there first dimension is the number of instances, the 2nd is the # 
# of input or output entries, and the 3rd dimension is number entries in 
# the sequence.
# 
def gen_cmdvec_9():
	inputvec = []
	bt = random.choice(range(0, 8))
	op = random.choice(['set','clear'])
	inputvec += [0]*8
	inputvec.append(1 if op == 'set' else 0)
	inputvec[bt] = 1
	return inputvec

def gen_instance(cls, seqlen):
	X = []
	Y = []
	cls.reset()
	for _ in range(seqlen):
		ivec = gen_cmdvec_9()
		X.append(ivec)
		if ivec[8] == 1:
			cls.set_input(ivec.index(1))
		else:
			cls.clear_input(ivec.index(1))
		#print(ivec, cls.output())
		Y.append(cls.output())
	return X, Y

def gen_dataset(cls, seqlen, number):
	Xs = []
	Ys = []
	count = 0
	while count < number:
		x, y = gen_instance(cls, seqlen)
		Xs.append(x)
		Ys.append(y)
		count += 1
	Xs = np.array(Xs, dtype=float)
	Ys = np.array(Ys, dtype=float)
	return Xs, Ys
	
if __name__ == '__main__':
	e = EightBitMachine()

	si = SimpleDirectInvertMachine(7)
	print(si.output())
	si.set_input(7)
	print(si.output())
	si.set_input(4)
	print(si.output())

	x = SimpleXORMachine(3,4)
	print(x.output())
	x.set_input(4)
	print(x.output())
	x.set_input(0)
	print(x.output())
	x.set_input(3)
	print(x.output())
	
	x, y = gen_dataset(e, 20, 2000)
	i = np.array(x)
	o = np.array(y)
	print(i.shape, o.shape)
