import numpy
import matplotlib.pyplot as plt
import pandas
import math
from keras.models import Sequential
from keras.layers import Dense, LSTM, TimeDistributedDense, TimeDistributed
from keras.callbacks import ModelCheckpoint
import numpy as np
import xormachine as xm
import sys

e = xm.SimpleXORMachine(2, 5)

x, y = xm.gen_dataset(e, 20, int(sys.argv[1]))
xt, yt = xm.gen_dataset(e, 20, 500)
X = np.array(x, dtype=float)
Y = np.array(y, dtype=float)

Xt = np.array(xt, dtype=float)
Yt = np.array(yt, dtype=float)

print(X.shape)
print(Y.shape)

model = Sequential()
#model.add(TimeDistributed(Dense(9), input_shape=(.argv[1]), 20, 9)))
model.add(LSTM(9, input_dim=9, input_length=20, activation='tanh', return_sequences=True))
#model.add(LSTM(8, input_dim=8, input_length=20, return_sequences=True))
#model.add(Dense(8))
model.add(TimeDistributed(Dense(1, activation='tanh')))
model.compile(loss='mse', optimizer='adam', metrics=["accuracy"])
#model.compile(loss='mse', optimizer='rmsprop', metrics=["accuracy"])

checkpointer = ModelCheckpoint(filepath="./xormachine.hdf5", verbose=1, save_best_only=True)

model.fit(X, Y, nb_epoch=10, batch_size=10, verbose=1, validation_split=0.1, callbacks=[checkpointer])

xt, yt = xm.gen_dataset(e, 20, 4)
Xt = np.array(xt, dtype=float)
Yt = np.array(yt, dtype=int)

count = 0
for _ in range(100):
	xt, yt = xm.gen_dataset(e, 20, 1)
	Xt = np.array(xt, dtype=float)
	Yt = np.array(yt, dtype=int)
	res = np.rint(model.predict(Xt, batch_size=1, verbose=0)).astype(int)
	if np.all(res[0] == Yt[0]):
		count += 1

print("%d out of 100 correctly predicted" % (count,))

#print(res2[0])
#print(Yt[0])
#print(res2[0] == Yt[0])


#model.evaluate(Xt, Yt, batch_size=10, verbose=1)
print("Done.")
