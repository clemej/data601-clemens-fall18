

class serial16550:
	def __init__(self):
		self.regs = { 'txbuf': 0, 'rxbuf': 0, 'dllb': 1, 'ier': 0, 
			'dlhb': 0, 'iir': 1, 'fcr': 0, 'lcr': 0, 'mcr': 0, 
			'lsr': 0, 'msr': 0, 'scratch': 0}

		self.reg_read_map = ['rxbuf','ier','iir','lcr','mcr',
							'lsr','msr','scratch']
		self.reg_write_map = ['txbuf','ier','fcr','lcr','mcr',
							None, None, 'scratch']

	def reset(self):
		self.regs = { 'txbuf': 0, 'rxbuf': 0, 'dllb': 1, 'ier': 0, 
			'dlhb': 0, 'iir': 1, 'fcr': 0, 'lcr': 0, 'mcr': 0, 
			'lsr': 0, 'msr': 0, 'scratch': 0}

	def baudrate(self):
		if self.regs['dlhb'] == 0 and self.regs['dllb'] == 0:
			return -1
		return int(115200 / ((self.regs['dlhb']*256) + self.regs['dllb'])) 

	def parity(self):
		if (self.regs['lcr'] & 8) == 0:
			return 'None'
		if (self.regs['lcr'] & 32) == 0:
			if (self.regs['lcr'] & 16) == 0:
				return 'Odd'
			else:
				return 'Even'
		if (self.regs['lcr'] & 16) == 0:
			return 'High'
		else:
			return 'Low'
	
	def wordlen(self):
		return [5, 6, 7, 8][self.regs['lcr'] & 3]

	def stopbits(self):
		if self.regs['lcr'] & 4 == 0:
			return 1
		
		if self.wordlen() == 5:
			return 1.5
		
		return 2

	def is_dlab(self):
		if (self.regs['lcr'] & 128) != 0:
			return True
		return False

	def status(self):
		return { 'baudrate': self.baudrate(),
			 'wordlen': self.wordlen(),
			 'parity': self.parity(), 
			 'stopbits': self.stopbits(),
			 'txbuf': (hex(self.regs['txbuf']),chr(self.regs['txbuf'])),
			 'rxbuf': (hex(self.regs['rxbuf']),chr(self.regs['rxbuf'])),
			 'scratch': self.regs['scratch'],
			 'regs': [hex(self.read(x)) for x in range(0,8)] }

	def __repr__(self):
		return str(self.status())

	def read(self, roffset):
		if roffset < 0 or roffset > 7:
			return

		if roffset < 2 and self.is_dlab():
			if roffset == 0: 
				return self.regs['dllb']
			return self.regs['dlhb']

		return self.regs[self.reg_read_map[roffset]]

	def write(self, roffset, byte):
		if roffset < 0 or roffset > 7:
			return

		self.regs['txbuf'] = 0

		if roffset < 2 and self.is_dlab():
			if roffset == 0: 
				self.regs['dllb'] = byte
				return
			self.regs['dlhb'] = byte
			return

		if self.reg_write_map[roffset] == None:
			return

		self.regs[self.reg_write_map[roffset]] = byte
	
	def set_bit(self, roffset, bit):
		if roffset < 0 or roffset > 7:
			return

		self.regs['txbuf'] = 0

		if roffset < 2 and self.is_dlab():
			if roffset == 0: 
				self.regs['dllb'] |= 2**bit
				return
			self.regs['dlhb'] |= 2**bit
			return

		if self.reg_write_map[roffset] == None:
			return

		self.regs[self.reg_write_map[roffset]] |= 2**bit

	def clear_bit(self, roffset, bit):
		if roffset < 0 or roffset > 7:
			return

		self.regs['txbuf'] = 0

		if roffset < 2 and self.is_dlab():
			if roffset == 0: 
				self.regs['dllb'] &= ~(2**bit)
				return
			self.regs['dlhb'] &= ~(2**bit)
			return

		if self.reg_write_map[roffset] == None:
			return

		self.regs[self.reg_write_map[roffset]] &= ~(2**bit)

	def toggle_bit(self, roffset, bit):
		if roffset < 0 or roffset > 7:
			return

		self.regs['txbuf'] = 0

		if roffset < 2 and self.is_dlab():
			if roffset == 0: 
				self.regs['dllb'] ^= 2**bit
				return
			self.regs['dlhb'] ^= 2**bit
			return

		if self.reg_write_map[roffset] == None:
			return

		self.regs[self.reg_write_map[roffset]] ^= 2**bit


class serialcmd:

	regnames = ['Tx / Rx / DLL', 'IER / DLH', 'IIR / FCR', 
			'LCR', 'MCR', 'LSR', 'MSR', 'scratch']
	 

	def __init__(self, cmd='write', reg=0, val=0):
		self.cmd = cmd
		self.reg = reg
		self.val = val
		self.vec = None

	def from_vec(self, v):
		if int(round(v[0])) == 1: 
			self.cmd = 'write'
		else:
			self.cmd = 'read'

		self.reg = int(round(v[1])*4 + round(v[2])*2 + round(v[3]))
		self.val = int(round(v[4])*128 + round(v[5])*64 + 
				round(v[6])*32 + round(v[7])*16 +
				round(v[8])*8 + round(v[9])*4 +
				round(v[10])*2 + round(v[11]))

	def to_vec(self):
		if self.vec != None:
			return self.vec

		self.vec = [0.0]*12
		if self.cmd == 'write':
			self.vec[0] = 1.0
		for idx,bit in enumerate(format(self.reg, "03b")):
			if bit == '1': self.vec[1+idx] = 1.0 
		if cmd[0] == 'write':
			for idx,bit in enumerate(format(self.val, "08b")):
				if bit == '1': self.vec[4+idx] = 1.0
	

	def as_list(self):
		return self.to_vec()

	def __repr__(self):
		if self.cmd == 'write':
			return '%s 0x%02x to ' % (self.cmd, 
					self.val, self.regnames[self.reg])
		else:
			return '%s from %s' % (self.cmd, 
					self.regnames[self.reg])





# for every key in dict target, there must be an equal values tag in 
# output.  Otherwise, return a value 
def fitness_func(output, target):
	totalkeys = float(len(target))
	count = 0.0
	for k in target:
		if output[k] == target[k]:
			count += 1.0
	return float(float(count)/float(totalkeys))

if __name__ == '__main__':
	import random
	import time
	import statistics
	import sys

	port = serial16550()

	#port.write(3, 0x80)
	#print(port.is_dlab())
	#print(port.regs)
	#port.write(0, 0x0c)
	#port.write(3, 0x00)
	#print(port.status())
	port.reset()

	target = {'baudrate': 9600, 'parity': 'None', 'wordlen': 8,
			'stopbits': 1, 'txbuf':  (hex(72),chr(72))}
	count = 1
	realcount = 0
	iterations = []
	start = time.clock()
	try:
		while realcount < int(sys.argv[1]):
			count = 1
			while True:
				#if count % 100000 == 0:
				#	print '10000 in ', time.clock() - start
				#	start = time.clock()
				#port.reset()
				# generate random input
				#for reg in range(8):
				port.write(random.randint(0,7), random.randint(0,255))
				ret = fitness_func(port.status(), target) 
				if ret == 1.0:
					#print count, ret, port.status()
					realcount += 1
					iterations.append(count)
					port.reset()
					break
				count += 1
				#end = time.clock()
				#print end-start
	except KeyboardInterrupt:
		print(count, port.status()) 

	print("Time for %s iterations: %f" % (sys.argv[1], time.clock()-start))
	print("min: %d, max: %d, mean: %f, stddev: %f" % (min(iterations), max(iterations),
				statistics.mean(iterations), statistics.stdev(iterations)))
	#iterations.sort()
	import matplotlib.pyplot as plt
	plt.hist(iterations)
	plt.title('Iterations Histogram')
	plt.xlabel('Iterations')
	plt.ylabel('Frequency')
	plt.show()
