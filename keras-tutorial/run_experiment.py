import experiments
import sys
import json
import boto3
import platform
import datetime
import multiprocessing
import traceback
import time
import os


if __name__ == '__main__':
	print("Loading Dataset") 

	# initialize AWS
	sqs = boto3.resource('sqs')

	# connect to queue
	recvq = sqs.get_queue_by_name(QueueName='lstm-worklist')
	sendq = sqs.get_queue_by_name(QueueName='lstm-results')

	# wait for requests
	msgcnt = 0
	while True:
		if os.path.exists("STOP"):
			print("Exiting due to STOP file!")
			break
		print("Waiting for message")
		for msg in recvq.receive_messages(MaxNumberOfMessages=1, WaitTimeSeconds=20):
			print("Processing work unit [%d]: " % (msgcnt) + msg.body)
			msgcnt += 1
			try:
				ret = json.loads(experiments.run_experiment_json(msg.body))
			except:
				print("Error processing message, continuing\n%s", traceback.format_exc())
				continue

			# pare down message to fit in 256K (aws limit)
			response = json.loads(msg.body)
			response['node'] = platform.node()
			response['type'] = ret['type']
			response['success'] = True
			#response['perf_val'] = ret['final_val_acc']
			response['time'] = ret['time']
			response['date'] = str(datetime.datetime.now())
			response['final_val_loss'] = ret['final_val_loss']
			response['final_loss'] = ret['final_loss']
			response['final_acc'] = ret['final_acc']
			response['final_val_acc'] = ret['final_val_acc']
			response['last5_loss'] = ret['last5_loss']
			response['opt'] = ret['opt']
			response['lossfunc'] = ret['lossfunc']
		
			resp = json.dumps(response)
			
			try:
				sendq.send_message(MessageBody=resp)
			except:
				print("Error sending response, continuing")
				continue

			msg.delete()

		print("Finished processing message, waiting 1 minutes and looping back");
		time.sleep(60);

