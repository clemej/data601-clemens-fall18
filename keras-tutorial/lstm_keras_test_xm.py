import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, LSTM, TimeDistributedDense, TimeDistributed
from keras.callbacks import ModelCheckpoint, EarlyStopping, Callback
import numpy as np
import xormachine as xm
import sys
import os
import pickle
from keras.utils.visualize_util import plot

class EarlyStopLess(Callback):
	def __init__(self, op='val_loss', epsilon=0.0001, verbose=0):
		super(EarlyStopLess, self).__init__()

		self.op = op
		self.epsilon = epsilon
		self.verbose = verbose

	def on_epoch_end(self, epoch, logs={}):
		val = logs.get(self.op)
		if val is None:
			import warnings
			warnings.warn("Required monitor %s not found" % 
						(self.op,), RuntimeWarning) 

		if val < self.epsilon:
			if self.verbose > 0:
				print('Epoch %d: %s = %f is less than %f\n' % 
					(epoch, self.op, val, self.epsilon))
			self.model.stop_training = True


def build_model_with_logger(n_in, n_out, seqlen, metrics=["accuracy"], nhidden=0):
	model = Sequential()
	model.add(LSTM(n_in, input_dim=n_in, 
				input_length=seqlen, 
				activation='tanh',
				#activation='hard_sigmoid', 
				return_sequences=True))
	#model.add(LSTM(8, return_sequences=True))
	for _ in range(nhidden):
		model.add(TimeDistributed(Dense(16, activation='tanh')))
	model.add(TimeDistributed(Dense(n_out, activation='tanh')))
	model.compile(loss='mse', optimizer='adam', metrics=["accuracy"])
	return model

machines = {
	'EightBitMachine': (xm.EightBitMachine(), 9, 8),
	'SingleDirectMachine': (xm.SingleDirectMachine(6), 9, 8),
	'SimpleDirectInvertMachine': (xm.SimpleDirectInvertMachine(3), 9, 8),
	'SimpleXORMachine': (xm.SimpleXORMachine(3,7), 9, 1),
	'ParityMachine': (xm.ParityMachine(), 9, 1)
}
		
if not sys.argv[1] in machines.keys():
	print('Invalid machine, valid machines are: %s\n' % machines.keys())
	sys.exit()

minfo = machines[sys.argv[1]]
n = int(sys.argv[2])
slen = int(sys.argv[3])
bsize = int(sys.argv[4])
nepochs = int(sys.argv[5])

print("Set to train network for machine: %s" % (sys.argv[1]))
print("Training params: batch_size = %d, epochs = %s" % (bsize, nepochs))
print("Generating %d Input/Output pairs" % (n,))

X, Y = xm.gen_dataset(minfo[0], slen, n)

print(" -- X: %s Y: %s" % (X.shape, Y.shape))
print("Compiling Model")

print(" -- Will save model to file: %s-<epochs>-<val_loss>.%d.hd5" % 
						(sys.argv[1], os.getpid()))
checkpointer = ModelCheckpoint(
	filepath="model.{epoch:02d}-{val_loss:.6f}.%d.hdf5" % (os.getpid(),), 
	verbose=0, save_best_only=True)

print(" -- Will stop training if val_loss doesn't decrease in after 5 epochs")
early_exit = EarlyStopping(patience=5)

print(" -- Will stop training if val_loss < 0.00001")
early_exit_loss = EarlyStopLess(op='val_loss', epsilon=0.00001, verbose=1)

model = build_model_with_logger(minfo[1], minfo[2], slen, nhidden=1)
model.summary()
#plot(model, to_file='%s.%d.png' % (sys.argv[1], os.getpid()))

print("Begining training.")
h = model.fit(X, Y, nb_epoch=nepochs, batch_size=bsize, verbose=2, 
		validation_split=0.1, 
		callbacks=[checkpointer, early_exit_loss, early_exit])

print("Saving model to model.%s.final.%d.hd5" % (sys.argv[1], os.getpid()))
model.save('model.%s.final.%d.hd5' % (sys.argv[1], os.getpid()))

print("Saving history to model.history.%d.p" % (os.getpid()))
with open("model.%s.history.%d.p" % (sys.argv[1], os.getpid()), "wb") as p:
	pickle.dump(h.history, p)

print("Checking predictions")
n_test = 1000
count = 0
for _ in range(n_test):
	Xt, Yt = [np.array(r, dtype=float) for r in 
					xm.gen_dataset(minfo[0], slen, 1)]
	res = np.rint(model.predict(Xt, batch_size=1, verbose=0)).astype(int)
	if np.all(res[0] == Yt[0]):
		count += 1

print("%d out of %d correctly predicted" % (count,n_test))



