from serialemu import serial16550
import random
import sys
import pickle
import gzip
import numpy as np

def gen_random_statement():
	#statements = ('write', 'read') # (ignore read since it can not change state)?
	inputs = {'cmds': ('read', 'write'), 
			'regs': tuple(range(0,8)), 
			'values': tuple(range(0,256)) }
	return (random.choice(inputs['cmds']), 
				random.choice(inputs['regs']),
				random.choice(inputs['values']))

def gen_program(proglen):
	return [gen_random_statement() for x in range(0, proglen)]

class command:
	def __init__(self, cmd='write', reg=0, val=0):
		self.cmd = cmd
		self.reg = reg
		self.val = val
		self.vec = None

	def from_vec(self, v):
		if int(round(v[0])) == 1: 
			self.cmd = 'write'
		else:
			self.cmd = 'read'

		self.reg = int(round(v[1])*4 + round(v[2])*2 + round(v[3]))
		self.val = int(round(v[4])*128 + round(v[5])*64 + 
				round(v[6])*32 + round(v[7])*16 +
				round(v[8])*8 + round(v[9])*4 +
				round(v[10])*2 + round(v[11]))

	def to_vec(self):
		if self.vec != None:
			return self.vec

		self.vec = [0.0]*12
		if self.cmd == 'write':
			self.vec[0] = 1.0
		for idx,bit in enumerate(format(self.reg, "03b")):
			if bit == '1': self.vec[1+idx] = 1.0 
		if cmd[0] == 'write':
			for idx,bit in enumerate(format(self.val, "08b")):
				if bit == '1': self.vec[4+idx] = 1.0
	

	def as_list(self):
		return self.to_vec()
# 
# Convert a program into an input encoding. Input encoding is:
# 1 bit read/write, 3 bits reg, 8 bits value = 12 input nodes. 
# Output encoding is:
# - 5 bit one-hot parity 
# - 4 bit one-hot wordlen
# - 3 bit one-hot stopbits
# - 6 standard baud rates, one-hot?  300/1200/2400/9600/56000/115200?
# 
def prog_to_vec(prog):
	X = []
	Y = []
	port = serial16550()
	port.reset()
	for cmd in prog:
		ivec = [0.0]*12
		if cmd[0] == 'write':
			ivec[0] = 1.0
		for idx,bit in enumerate(format(cmd[1], "03b")):
			if bit == '1': ivec[1+idx] = 1.0 
		if cmd[0] == 'write':
			for idx,bit in enumerate(format(cmd[2], "08b")):
				if bit == '1': ivec[4+idx] = 1.0
		# same input vec for command.
		X.append(ivec)
		
		# run instruction
		if cmd[0] == 'write':
			port.write(cmd[1], cmd[2])

		# calculate Y
		ovec = [0.0]*13
		status = port.status()
		ovec[['None', 'Even', 'Odd', 'High', 'Low'].index(status['parity'])] = 1.0
		ovec[5+[5,6,7,8].index(status['wordlen'])] = 1.0
		ovec[9+[1,1.5,2].index(status['stopbits'])] = 1.0
		ovec[12] = float(status['baudrate'])/115200.0
		#bauds = [300, 1200, 2400, 9600, 57600, 115200]
		#if status['baudrate'] in bauds:
		#	ovec[12+bauds.index(status['baudrate'])] = 1.0
		#else:
		#	ovec[18] = 1.0
		Y.append(ovec)
		
	return X, Y

def gen_dataset(cls, proglen, size):
	Xs = []
	Ys = []
	count = 0
	while count < size:
		prog = gen_program(proglen)
		x, y = prog_to_vec(prog)
		Xs.append(x)
		Ys.append(y)
		count += 1
	Xs = np.array(Xs)
	Ys = np.array(Ys)
	return Xs, Ys

if __name__ == '__main__':
	Xs, Ys = gen_dataset(None, int(sys.argv[1]), int(sys.argv[2]))
	Xs = np.array(Xs)
	Ys = np.array(Ys)
	print(Xs.shape, Ys.shape)
	print(Xs[0])
	print(Ys[0])

	pickle.dump((Xs, Ys), gzip.open("lstm-serial-dataset-%d-%d.p.gz" % (int(sys.argv[1]), int(sys.argv[2])),"wb"))
